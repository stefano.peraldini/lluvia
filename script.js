"use strict";

const API_KEY = "d9a7697af0356a38b7061602148c1cd9";
const RAIN_MARGIN = 8;

const permission = document.querySelector(".permision");
const positive = document.querySelector(".positive");
const negative = document.querySelector(".negative");
const error = document.querySelector(".error");

function showPanel(panel) {
  panel.classList.remove("hidden");
}

function hideAllPanel() {
  permission.classList.add("hidden");
  positive.classList.add("hidden");
  negative.classList.add("hidden");
  error.classList.add("hidden");
}

function getUserLocation() {
  hideAllPanel();
  navigator.geolocation.getCurrentPosition(
    (location) => {
      const { latitude, longitude } = location.coords;
      getWeatherData({ latitude, longitude });
      localStorage.setItem("permission", "ok");
    },
    () => {
      showPanel(error);
    }
  );
}

async function getData({ url, option = {} }) {
  const response = await fetch(url, option);

  if (!response.ok) {
    throw new Error("Error en la petición");
  }

  return response.json();
}

async function getWeatherData({ latitude, longitude }) {
  try {
    const currentWeather = await getData({
      url: `https://api.openweathermap.org/data/2.5/weather?lat=${latitude}&lon=${longitude}&units=metric&lang=es&appid=${API_KEY}`,
    });
    console.log(currentWeather);
    const nextHours = await getData({
      url: `https://api.openweathermap.org/data/2.5/onecall?lat=${latitude}&lon=${longitude}&units=metric&lang=es&exclude={current,minutely,daily,alerts}&appid=${API_KEY}`,
    });
    console.log(nextHours);
    const nextRain = nextHours.hourly.findIndex((hour) => {
      return hour.weather[0].main === "Rain";
    });
    console.log(nextRain);

    if (nextRain > -1 && nextRain <= RAIN_MARGIN) {
      showPositive({
        city: currentWeather.name,
        temperature: currentWeather.main.temp,
        weather: currentWeather.weather[0].description,
        nextRain,
      });
    } else {
      showNegative({
        city: currentWeather.name,
        temperature: currentWeather.main.temp,
        weather: currentWeather.weather[0].description,
      });
    }
  } catch (error) {
    showPanel(error);
  }
}

function showPositive({ city, temperature, weather, nextRain }) {
  showPanel(positive);
  const text = positive.querySelector("p");
  text.innerHTML = `Ahora mismo hay ${temperature}°C en ${city} con ${weather} y
    ${
      nextRain > 0
        ? `probabilmente llueve en ${nextRain} horas`
        : `está lloviendo ahora mismo`
    }
    `;
}

function showNegative({ city, temperature, weather }) {
  showPanel(negative);
  const text = negative.querySelector("p");
  text.innerHTML = `Ahora mismo hay ${temperature}°C en ${city} con ${weather} y parece que no lloverá en la próximas ${RAIN_MARGIN} horas`;
}

function main() {
  if (localStorage.getItem("permission") === "ok") {
    getUserLocation();
  } else {
    showPanel(permission);
    const permissionButton = document.querySelector("button");

    permissionButton.addEventListener("click", () => {
      getUserLocation();
    });
  }
}

main();
